from variant import func_n
import pytest

@pytest.mark.parametrize("N, F", [(124, 421), (54786, 68745), (4 , 4)])


def test_good(N, F):
    assert func_n(N) == F

@pytest.mark.parametrize("N, F", [(4568, 8645), (12, 22), (4 , 1)])

def test_bad(N, F):
    assert func_n(N) == F